Implementação 
========

	Nosso projeto está estruturado em pacotes, que são responsáveis por suas funções especificas em cada classe.
	No pacote /br/ufrn/datastructure/ estão as classes que contém as estruturas fundamentais para a solução do problema, sendo elas: Edge, que será a classe que contém a estrutura para as arestas; Unode que será o Node referente aos conjuntos disjuntos; Node que contém a estrutura da melhor arvore gerada para a solução.
	No pacote /br/ufrn/TAD/  estão as classes referentes as estruturas de dados heapMin e conjuntos disjuntos, sendo elas: FilaPrioridade é uma interface que possui as assinaturas de metodos para a filaprioridadeHeap

