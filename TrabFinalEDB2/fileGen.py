from random import randint
import sys

I = 0

def generateFile():
	global I
	I = I+1
	path = "in"+str(I)	
	arq = open(path,'w')

	LIM_INF_CUSTO = randint(10,100)
	LIM_SUP_CUSTO = LIM_INF_CUSTO + randint(10,100)
	N = 10
	D = 2

	arq.write("%d %d\n" % (N,D))

	for i in range(1,N):
		for j in range(i+1,N+1):
			arq.write("%d " % randint(LIM_INF_CUSTO,LIM_SUP_CUSTO))
		arq.write("\n")

def generateFiles(n):
	for i in range(n):
		generateFile()

print ">>> ",
x = input()
generateFiles(x)
