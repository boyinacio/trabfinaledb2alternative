package br.ufrn.datastructure;

public class UNode<T> {
	
	private T data;
	private UNode<T> pai;
	private int rank;
	
	public UNode(){
		data = null;
		pai = this;
		rank = 0;
	}

	public T getData() {
		return data;
	}

	public UNode<T> getPai() {
		return pai;
	}

	public int getRank() {
		return rank;
	}

	public void setData(T data) {
		this.data = data;
	}

	public void setPai(UNode<T> pai) {
		this.pai = pai;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}
	
}
