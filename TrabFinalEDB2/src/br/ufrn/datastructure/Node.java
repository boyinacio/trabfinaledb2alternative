package br.ufrn.datastructure;

import java.util.ArrayList;

public class Node {

	private int grau;
	private int label;
	private ArrayList<Node> filhos;
	private Node pai;
	
	public Node(int d) {
		grau = d;
		filhos = new ArrayList<Node>();
		pai = null;
	}

	public Node getPai() {
		return pai;
	}

	public void setPai(Node pai) {
		this.pai = pai;
	}

	public int getGrau() {
		return grau;
	}

	public void setGrau(int grau) {
		this.grau = grau;
	}
	
	public void addFilho(Node no){
		if(filhos.size() < grau){
			this.filhos.add(no);
		}
		
		
	}

	public ArrayList<Node> getFilhos() {
		return filhos;
	}

	public void setFilhos(ArrayList<Node> filhos) {
		this.filhos = filhos;
	}

	public int getLabel() {
		return label;
	}

	public void setLabel(int label) {
		this.label = label;
	}

}
