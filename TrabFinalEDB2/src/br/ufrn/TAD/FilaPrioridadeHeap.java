package br.ufrn.TAD;

import java.util.ArrayList;

public class FilaPrioridadeHeap<E extends Comparable<E>> implements FilaPrioridade<E> {

	private ArrayList<E> L;
	
	public FilaPrioridadeHeap(){
		L = new ArrayList<E>();
	}
	
	private int parent(int i){
		if(i % 2 != 0){
			i++;
		}
		return (i/2) - 1;
	}
	 
	private int leftChild(int i){
		return (2*i) + 1;
	}
	
	private int rightChild(int i){
		return (2*i) + 2;
	}
	
	@Override
	public void insert(E elem) {
		L.add(elem);
		if(L.size() > 1){
			int actual = L.size()-1;
			int parent = parent(actual);
			 
			while(parent >= 0){
				if(L.get(actual).compareTo(L.get(parent)) < 0){
					// Swaps parent and child
					swap(parent,actual);
					actual = parent;
					parent = parent(actual);
					
				} else {
					break;
				}
			}		
		}
	}

	@Override
	public E max() {
		if(L.size() > 0){
			return L.get(0);
		}
		return null;
	}

	@Override
	public E extractMax() {
		if(L.size() == 0){
			return null;
		}
		
		E max = L.get(0);
		swap(0,L.size()-1);		
		L.remove(L.size()-1);
		
		if(L.size() > 0){
			heapify(0);
		}
		
		return max;
	}
	
	private void heapify(int node){
		E max = L.get(node);
		int indMax = node;
		if((leftChild(node) < L.size()) && (L.get(leftChild(node)).compareTo(max) < 0)){
			indMax = leftChild(node);
			max = L.get(leftChild(node));
		}
		if((rightChild(node) < L.size()) && (L.get(rightChild(node)).compareTo(max) < 0)){
			indMax = rightChild(node);
		}
		if(indMax != node){
			swap(node,indMax);
			heapify(indMax);
		}
	}
	
	private void swap(int i, int j){
		E aux = L.get(i);
		L.set(i, L.get(j));
		L.set(j, aux);
	}
	
	public int size(){
		return L.size();
	}
	
}
