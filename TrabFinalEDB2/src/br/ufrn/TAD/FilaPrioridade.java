package br.ufrn.TAD;

public interface FilaPrioridade<E extends Comparable<E>> {

	public void insert(E elem);
	public E max();
	public E extractMax();

}
