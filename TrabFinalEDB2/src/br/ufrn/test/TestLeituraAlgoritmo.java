package br.ufrn.test;

import java.io.File;
import java.util.ArrayList;

import br.ufrn.TAD.FilaPrioridade;
import br.ufrn.algorithm.Algorithm;
import br.ufrn.algorithm.LerDados;
import br.ufrn.datastructure.Edge;

public class TestLeituraAlgoritmo {

	public static void main(String[] args) {
		int NUM_ARQS = 3;
		for (int i = 1; i <= NUM_ARQS; i++) {
			System.out.println("******************************");
			tester(i);
		}

	}
	
	public static void tester(int i){
		File arquivo = new File("in"+i);

		Object[] data = LerDados.lerDados(arquivo);
		
		int n = (int) data[0];
		int d = (int) data[1];
		
		FilaPrioridade<Edge> fila = (FilaPrioridade<Edge>) data[2];
		
		ArrayList<Edge> arestas = Algorithm.selectEdges(fila, n, d);
		int custo = 0;

		System.out.println("Número de arestas: " + arestas.size());
		for (Edge aresta : arestas) {
			System.out.println(aresta.toString());
			custo += aresta.getCost();
		}

		System.out.println("Custo total: " + custo);
	}
	
}
