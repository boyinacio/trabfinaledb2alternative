package br.ufrn.test;

import java.io.File;

import br.ufrn.TAD.FilaPrioridade;
import br.ufrn.algorithm.LerDados;
import br.ufrn.datastructure.Edge;

public class TestLeitura {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		File arquivo = new File("in");
		Object[] data = LerDados.lerDados(arquivo);
		
		int n = (int) data[0];
		int d = (int) data[1];
		
		FilaPrioridade<Edge> fila = (FilaPrioridade<Edge>) data[2];
		
		System.out.println("N: " + n + " | D: " + d);
		
		Edge E;
		
		while ((E = fila.extractMax()) != null) {
			System.out.println(E.toString());
		}
		
	}

}
