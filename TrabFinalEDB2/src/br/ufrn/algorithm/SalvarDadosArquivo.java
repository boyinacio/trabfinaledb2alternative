package br.ufrn.algorithm;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.File;
import java.util.ArrayList;

import br.ufrn.datastructure.Edge;
import br.ufrn.datastructure.Node;


public class SalvarDadosArquivo {

	private FileWriter arq;
	private FileWriter arq2;
	private PrintWriter gravarArq;
	private PrintWriter gravarArq2;
	private BuildTree resultado;
	private BuildTree resultado2;
	private ArrayList<String> arquivo = new ArrayList<String>();
	private ArrayList<String> arquivo2 = new ArrayList<String>();
	private ArrayList<String> edges = new ArrayList<String>();
	private int custo = 0;

	public void saveData(Node root, ArrayList<Edge> arestas){
		resultado = new BuildTree();
		arquivo = resultado.printTree2(root);
		edges = saveDataEdges(root, arestas);
		custo = custo(arestas, custo);
		try {
			arq = new FileWriter("ArquivoSaida.txt");
			gravarArq  = new PrintWriter(arq);
			gravarArq.print("Numero de arestas: " + arestas.size() + "\n");

			for (int j = 0; j < edges.size(); j++) {
				gravarArq.printf(edges.get(j));
			}


			for (int i = 0; i < arquivo.size(); i++) {
				gravarArq.printf(arquivo.get(i));
			}
			gravarArq.println("Custo total: " + custo);
			//gravarArq.printf(arquivo);
			arq.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public ArrayList<String> saveDataEdges(Node root, ArrayList<Edge> arestas){
		resultado2 = new BuildTree();
		arquivo2 = resultado2.printTree2(root);
		for (Edge aresta : arestas) {
			edges.add(aresta.toString() + "\n");
		}
		//gravarArq2.printf(arquivo2);
		//arq2.close();
		return edges;
	}


	public int custo(ArrayList<Edge> arestas, int custo){
		for (Edge aresta : arestas) {
			custo += aresta.getCost();
		}
		return custo;
	}


}
