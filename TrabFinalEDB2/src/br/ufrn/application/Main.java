package br.ufrn.application;

import java.io.File;
import java.util.ArrayList;

import javax.swing.JFileChooser;

import br.ufrn.TAD.FilaPrioridade;
import br.ufrn.algorithm.Algorithm;
import br.ufrn.algorithm.BuildTree;
import br.ufrn.algorithm.LerDados;
import br.ufrn.algorithm.SalvarDadosArquivo;
import br.ufrn.datastructure.Edge;
import br.ufrn.datastructure.Node;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		File selected = null;
		JFileChooser chooser = null;

		String url = ".";

		chooser = new JFileChooser(url);
		chooser.setAcceptAllFileFilterUsed(false);
		chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
			selected = chooser.getSelectedFile();
		}

		Object[] data = LerDados.lerDados(selected);

		int n = (int) data[0];
		int d = (int) data[1];

		
		FilaPrioridade<Edge> arestas = (FilaPrioridade<Edge>) data[2];
		ArrayList<Edge> arestasR;
		
//		Algorithm solver = new Algorithm(arestas);
		
		long time = System.currentTimeMillis();
		arestasR = Algorithm.selectEdges(arestas, n, d);
		time = System.currentTimeMillis()-time;
		
		int custo = 0;

		System.out.printf("Duração do algoritmo: %d milissegundos\n",time);
		System.out.println("Número de arestas: " + arestasR.size());
		for (Edge aresta : arestasR) {
			System.out.println(aresta.toString());
			custo += aresta.getCost();
		}

		Node raiz = BuildTree.arvore(arestasR, d, n);
		BuildTree.printTree(raiz);

		SalvarDadosArquivo salvar = new SalvarDadosArquivo();
		SalvarDadosArquivo salvarEdge = new SalvarDadosArquivo();
		salvar.saveData(raiz, arestasR);
		salvarEdge.saveDataEdges(raiz, arestasR);

		System.out.println("Custo total: " + custo);

	}

}
